package ru.semenov.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import ru.semenov.domain.model.SymbolEntity;
import ru.semenov.dto.SymbolEntityResponseFrequency;
import ru.semenov.repository.SymbolEntityRepository;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SymbolEntityRestControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private SymbolEntityRepository symbolEntityRepository;

    @Test
    public void testFrequencyEndpointConcurrent() throws Exception {
        int threadCount = 100;
        String inputString = "aaabbcc";
        SymbolEntity symbolEntity = new SymbolEntity();
        symbolEntity.setName(inputString);
        symbolEntity.setFrequency(BigInteger.ONE);
        symbolEntityRepository.save(symbolEntity);
        Map<String,BigInteger> expectedResponse = new HashMap<String,BigInteger>();
        for (int i = 0; i < inputString.length(); i++) {
            String key = String.valueOf(inputString.charAt(i));
            BigInteger value = expectedResponse.getOrDefault(key, BigInteger.ZERO).add(BigInteger.ONE);
            expectedResponse.put(key, value);
        }
        // Создаем пул потоков с 10 потоками
        ExecutorService executorService = Executors.newFixedThreadPool(threadCount);
        // Создаем список для хранения результатов выполнения потоков
        List<Future<SymbolEntityResponseFrequency>> results = new ArrayList<>();
        // Запускаем эндпоинт в каждом потоке
        for (int i = 0; i < threadCount; i++) {
            results.add(executorService.submit(() -> {
                MvcResult result = mockMvc.perform(get("/api/v1/symbols/frequency")
                                .param("inputString", inputString))
                        .andExpect(status().isOk())
                        .andReturn();
                String jsonResponse = result.getResponse().getContentAsString();
                ObjectMapper objectMapper = new ObjectMapper();
                return objectMapper.readValue(jsonResponse, SymbolEntityResponseFrequency.class);
            }));
        }
        // Ожидаем завершение всех потоков
        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        // Проверяем результаты каждого потока
        for (Future<SymbolEntityResponseFrequency> result : results) {
            SymbolEntityResponseFrequency actualResponse = result.get();
            assertEquals(expectedResponse, actualResponse.getFrequencyMap());
        }
    }

}
