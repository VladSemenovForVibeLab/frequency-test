package ru.semenov.domain.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Builder
@Table(name = SymbolEntity.TABLE_NAME)
public class SymbolEntity implements Serializable,Comparable<SymbolEntity> {
    public static final String TABLE_NAME = "symbols";
    private static final String SYMBOL_ENTITY_ID_COLUMN_NAME = "id";
    private static final String SYMBOL_ENTITY_NAME_COLUMN_NAME = "name";
    private static final String SYMBOL_ENTITY_FREQUENCY_COLUMN_NAME = "frequency";
    private static final String SYMBOL_ENTITY_DATE_CREATED_COLUMN_NAME = "date_created";
    private static final String SYMBOL_ENTITY_DATE_UPDATED_COLUMN_NAME = "date_updated";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "uuid",strategy = "uuid2")
    @Setter(AccessLevel.NONE)
    @Column(name = SYMBOL_ENTITY_ID_COLUMN_NAME,nullable = false,unique = true)
    private String id;
    @Column(name = SYMBOL_ENTITY_NAME_COLUMN_NAME,nullable = false)
    private String name;
    @Column(name = SYMBOL_ENTITY_FREQUENCY_COLUMN_NAME,nullable = false)
    private BigInteger frequency;
    @Transient
    private final Object lock = new Object();
    @Column(name = SYMBOL_ENTITY_DATE_CREATED_COLUMN_NAME,nullable = false)
    public LocalDateTime dateCreated;
    @Column(name = SYMBOL_ENTITY_DATE_UPDATED_COLUMN_NAME,nullable = false)
    public LocalDateTime dateUpdated;
    @PrePersist
    public void onCreate(){
        this.dateCreated=LocalDateTime.now();
        this.dateUpdated=LocalDateTime.now();
    }
    @PreUpdate
    public void onUpdate(){
        this.dateUpdated=LocalDateTime.now();
    }

    public void addToFrequency(BigInteger value){
        synchronized (lock){
            this.frequency=this.frequency.add(value);
        }
    }

    @Override
    public int compareTo(SymbolEntity symbolEntity) {
        return this.frequency.compareTo(symbolEntity.frequency);
    }
}
