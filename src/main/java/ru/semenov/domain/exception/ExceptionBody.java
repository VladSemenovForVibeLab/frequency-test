package ru.semenov.domain.exception;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class ExceptionBody {
    private String message;
    private String code;
    private String status;
    private String path;
    private String timestamp;
    private String traceId;
    private String requestId;
    private String statusCode;
    private String error;

}
