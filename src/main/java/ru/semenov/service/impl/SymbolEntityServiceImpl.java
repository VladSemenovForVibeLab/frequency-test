package ru.semenov.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.domain.model.SymbolEntity;
import ru.semenov.dto.SymbolEntityResponseFrequency;
import ru.semenov.repository.SymbolEntityRepository;
import ru.semenov.service.ISymbolEntityService;

import java.math.BigInteger;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class SymbolEntityServiceImpl implements ISymbolEntityService {
    private final SymbolEntityRepository symbolEntityRepository;

    public SymbolEntityServiceImpl(SymbolEntityRepository symbolEntityRepository) {
        this.symbolEntityRepository = symbolEntityRepository;
    }

    @Override
    @Transactional
    public SymbolEntityResponseFrequency getFrequency(String inputString) {
        Map<String, BigInteger> frequencyMap = Arrays.stream(inputString.split(""))
                .collect(Collectors.toMap(Function.identity(), s -> BigInteger.ONE, BigInteger::add))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new
                ));
        List<SymbolEntity> symbolEntities = frequencyMap.entrySet()
                .stream()
                .map(e -> SymbolEntity.builder()
                        .name(e.getKey())
                        .frequency(e.getValue())
                        .build())
                .toList();
        synchronized (SymbolEntity.class) {
            symbolEntityRepository.saveAll(symbolEntities);
        }
        return new SymbolEntityResponseFrequency(frequencyMap);
    }
}
