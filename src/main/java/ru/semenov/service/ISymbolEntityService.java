package ru.semenov.service;

import ru.semenov.dto.SymbolEntityResponseFrequency;

public interface ISymbolEntityService {
    SymbolEntityResponseFrequency getFrequency(String inputString);
}
