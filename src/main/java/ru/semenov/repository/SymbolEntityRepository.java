package ru.semenov.repository;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import ru.semenov.domain.model.SymbolEntity;

import java.util.List;
import java.util.Objects;

@Repository
@RepositoryRestResource
public interface SymbolEntityRepository extends JpaRepository<SymbolEntity, String> {
    @Override
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    <S extends SymbolEntity> List<S> saveAll(Iterable<S> entities);
}