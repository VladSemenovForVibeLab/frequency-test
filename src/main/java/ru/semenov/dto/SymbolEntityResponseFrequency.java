package ru.semenov.dto;

import lombok.*;

import java.math.BigInteger;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class SymbolEntityResponseFrequency {
    Map<String, BigInteger> frequencyMap;
    public SymbolEntityResponseFrequency(Map<String, BigInteger> frequencyMap){
        this.frequencyMap = frequencyMap;
    }
}
