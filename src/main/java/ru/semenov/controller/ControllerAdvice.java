package ru.semenov.controller;

import org.hibernate.PropertyValueException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.semenov.domain.exception.ExceptionBody;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(PropertyValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handlePropertyValueException(PropertyValueException e) {
        return ExceptionBody
                .builder()
                .message("Вы передали неверные параметры!!")
                .status(HttpStatus.NOT_FOUND.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }

    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return ExceptionBody
                .builder()
                .message("Illegal state exception!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleSQLIntegrityConstraintViolation(SQLIntegrityConstraintViolationException e) {
        return ExceptionBody
                .builder()
                .message("SQL ошибки!")
                .status(HttpStatus.BAD_REQUEST.toString())
                .code("400")
                .path(e.getLocalizedMessage())
                .timestamp(LocalDateTime.now().toString())
                .traceId(e.getStackTrace().toString())
                .build();
    }
}

