package ru.semenov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.semenov.dto.SymbolEntityResponseFrequency;
import ru.semenov.service.ISymbolEntityService;

@RestController
@RequestMapping(SymbolEntityRestController.SYMBOL_ENTITY_REST_CONTROLLER_URI)
public class SymbolEntityRestController {
    public static final String SYMBOL_ENTITY_REST_CONTROLLER_URI="api/v1/symbols";
    private final ISymbolEntityService symbolEntityService;

    public SymbolEntityRestController(ISymbolEntityService symbolEntityService) {
        this.symbolEntityService = symbolEntityService;
    }
    @GetMapping("/frequency")
    public ResponseEntity<SymbolEntityResponseFrequency> getSymbolEntityFrequency(@RequestParam String inputString){
        synchronized (this){
            return new ResponseEntity<SymbolEntityResponseFrequency>(symbolEntityService.getFrequency(inputString), HttpStatus.OK);
        }
    }
}
